$(function () {
    $(".sidebar li > a").on('click', function (e) {
        // 
        $('.sidebar ul ul').slideUp(), $(this).next().is(':visible') || $(this).next().slideDown(), e.stopPropagation();
    })


    $(window).bind("load resize", function () {
        if ($(this).width() < 768) {
            $('.sidebar-collapse').addClass('collapse');
        } else {
            $('.sidebar-collapse').removeClass('collapse');
        }
    })
})